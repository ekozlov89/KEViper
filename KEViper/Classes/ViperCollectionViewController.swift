//
//  ViperCollectionViewController.swift
//  KEViper
//
//  Created by Kozlov Evgenii on 26/09/2018.
//

import UIKit

open class ViperCollectionViewController: UICollectionViewController, ViperModuleTransitionHandler {
    
    open var moduleInput: ViperModuleInput? {
        return nil
    }
    
    open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        handle(segue: segue, wrapper: sender)
    }
    
}
