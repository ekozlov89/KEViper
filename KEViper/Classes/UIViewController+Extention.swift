//
//  UIViewController+Extention.swift
//  KEViper
//
//  Created by Kozlov Evgenii on 26/09/2018.
//

import UIKit

extension UIViewController {
    
    static var defaultStoryboardName: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    func bypassNavigationController() -> UIViewController? {
        if let navigationController = self as? UINavigationController {
            return navigationController.viewControllers.first
        }else {
            return self
        }
    }
    
}
