//
//  ViperModuleTransitionHandler.swift
//  KEViper
//
//  Created by Kozlov Evgenii on 26/09/2018.
//

import UIKit


public enum ViperPresentationType {
    case modal(animated: Bool, completion: (() -> ())?)
    case push(animated: Bool)
}

public typealias ConfigurationBlock = (ViperModuleInput) -> ()

public class SegueConfigBlockWrapper: NSObject {
    public var configurationBlock: ConfigurationBlock? = nil
    public var configurator: ViperModuleConfigurator? = nil
    public var transitioningDelegate: UIViewControllerTransitioningDelegate? = nil
}

public protocol ViperModuleTransitionHandler: AnyObject {
    var moduleInput: ViperModuleInput? { get }
    
    func openModule(defaultSegueIdentifierFor: UIViewController.Type, configurator: ViperModuleConfigurator?, customTransitioningDelegate: UIViewControllerTransitioningDelegate?, configurationBlock: ConfigurationBlock?)
    
    func openModule(customSegueIdentifier: String, configurator: ViperModuleConfigurator?, customTransitioningDelegate: UIViewControllerTransitioningDelegate?, configurationBlock: ConfigurationBlock?)
    
    func openModule(viewController: UIViewController.Type, presentationType: ViperPresentationType, configurator: ViperModuleConfigurator?, customTransitioningDelegate: UIViewControllerTransitioningDelegate?, configurationBlock: ConfigurationBlock?)
    
    func openModule(viewController: UIViewController, presentationType: ViperPresentationType, configurator: ViperModuleConfigurator?, customTransitioningDelegate: UIViewControllerTransitioningDelegate?, configurationBlock: ConfigurationBlock?)
    
    func closeModule(animated: Bool, completion: (() -> ())?)
}

public extension ViperModuleTransitionHandler where Self: UIViewController {
    
    func openModule(defaultSegueIdentifierFor: UIViewController.Type, configurator: ViperModuleConfigurator? = nil, customTransitioningDelegate: UIViewControllerTransitioningDelegate? = nil, configurationBlock: ConfigurationBlock? = nil) {
        openModule(customSegueIdentifier: defaultSegueIdentifierFor.defaultStoryboardName, configurator: configurator, customTransitioningDelegate: customTransitioningDelegate, configurationBlock: configurationBlock)
    }
    
    func openModule(customSegueIdentifier: String, configurator: ViperModuleConfigurator? = nil, customTransitioningDelegate: UIViewControllerTransitioningDelegate? = nil, configurationBlock: ConfigurationBlock? = nil) {
        let wrapper: SegueConfigBlockWrapper = SegueConfigBlockWrapper()
        wrapper.configurationBlock = configurationBlock
        wrapper.configurator = configurator
        wrapper.transitioningDelegate = customTransitioningDelegate
        performSegue(withIdentifier: customSegueIdentifier, sender: wrapper)
    }
    
    func openModule(viewController: UIViewController.Type, presentationType: ViperPresentationType, configurator: ViperModuleConfigurator? = nil, customTransitioningDelegate: UIViewControllerTransitioningDelegate? = nil, configurationBlock: ConfigurationBlock? = nil) {
        openModule(viewController: viewController.init(), presentationType: presentationType, configurator: configurator, customTransitioningDelegate: customTransitioningDelegate, configurationBlock: configurationBlock)
    }
    
    func openModule(viewController: UIViewController, presentationType: ViperPresentationType, configurator: ViperModuleConfigurator? = nil, customTransitioningDelegate: UIViewControllerTransitioningDelegate? = nil, configurationBlock: ConfigurationBlock? = nil) {
        
        configurator?.configure(with: viewController)
        customTransitioningDelegate.flatMap{
            viewController.modalPresentationStyle = UIModalPresentationStyle.custom
            viewController.transitioningDelegate = $0
        }
        
        if let transitionHandler = viewController as? ViperModuleTransitionHandler {
            transitionHandler.moduleInput.flatMap{ configurationBlock?($0) }
        }
        
        switch presentationType {
        case ViperPresentationType.modal(animated: let animated, completion: let completion):
            present(viewController, animated: animated, completion: completion)
        case ViperPresentationType.push(animated: let animated):
            navigationController?.pushViewController(viewController, animated: animated)
        }
    }
    
    func closeModule(animated: Bool, completion: (() -> ())?) {
        navigationController?.popViewController(animated: true)
        
        if (presentingViewController != nil) {
            dismiss(animated: true, completion: completion)
        }
    }
    
}

extension ViperModuleTransitionHandler where Self: UIViewController {
    
    public func handle(segue: UIStoryboardSegue, wrapper: Any?) {
        guard let transitionHandler = segue.destination.bypassNavigationController() as? ViperModuleTransitionHandler else { return }
        guard let wrapper = wrapper as? SegueConfigBlockWrapper else { return }
        
        if let viewController = transitionHandler as? UIViewController {
            wrapper.configurator?.configure(with: viewController)
            wrapper.transitioningDelegate.flatMap{
                viewController.modalPresentationStyle = UIModalPresentationStyle.custom
                viewController.transitioningDelegate = $0
            }
        }
        
        transitionHandler.moduleInput.flatMap{ wrapper.configurationBlock?($0) }
    }
    
    func configure(with configurator: ViperModuleConfigurator?, transitioningDelegate: UIViewControllerTransitioningDelegate?, configurationBlock: @escaping ConfigurationBlock) {
        configurator?.configure(with: self)
        transitioningDelegate.flatMap{
            self.modalPresentationStyle = UIModalPresentationStyle.custom
            self.transitioningDelegate = $0
        }
        moduleInput.flatMap{ configurationBlock($0) }
    }
    
}


