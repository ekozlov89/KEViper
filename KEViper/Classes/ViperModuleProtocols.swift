//
//  ViperModuleProtocols.swift
//  KEViper
//
//  Created by Kozlov Evgenii on 26/09/2018.
//

import UIKit

public protocol ViperModuleInput: AnyObject {
    func setOutput(_ output: ViperModuleOutput?)
}

public extension ViperModuleInput {
    func setOutput(_ output: ViperModuleOutput?) {}
}

public protocol ViperModuleOutput: AnyObject {}

public protocol ViperModuleConfigurator {
    func configure(with viewInput: UIViewController)
}
