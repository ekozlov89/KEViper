# KEViper

[![CI Status](https://img.shields.io/travis/Kozlov Evghenii/KEViper.svg?style=flat)](https://travis-ci.org/Kozlov Evghenii/KEViper)
[![Version](https://img.shields.io/cocoapods/v/KEViper.svg?style=flat)](https://cocoapods.org/pods/KEViper)
[![License](https://img.shields.io/cocoapods/l/KEViper.svg?style=flat)](https://cocoapods.org/pods/KEViper)
[![Platform](https://img.shields.io/cocoapods/p/KEViper.svg?style=flat)](https://cocoapods.org/pods/KEViper)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KEViper is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'KEViper'
```

## Author

Kozlov Evghenii, ekozlov89@mail.ru

## License

KEViper is available under the MIT license. See the LICENSE file for more info.
